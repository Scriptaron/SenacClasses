### IT Jobs

- CAO - Chief Analytics Officer or Chief Administrative Officer

- CEO - Chief Executive Officer

- CDO - Chief Data Officer

- CFO - Chief Financial Officer

- CSPM - Cloud Security Posture Management

- DAST - Dynamic Application Security Testing 

- DevOps - Development Operations

- DFIR - Digital Forensics and Incident Response

- ED - Executive Director

- IAM - Identity and Access

- MDR - Managed Detection and Response

- MSSP - Managed Security Service Provider

- VC - Vice Chancellor

- VP - abbreviation for Vice President

- WAF - Web Application Firewall

### IT Related

- 2FA - 2 Factor Authentication

- AI - Artificial Intelligence

- AV - Antivirus

- CSS - Cascading Style Sheets

- DDoS - Distributed Denial of Service

- DNS - Domain Name Server:

- HTTPS - Secure Hypertext Transfer Protocol

- HTML - HyperText Markup Language

- IOT - Internet of Things

- IP - Intellectual Property

- IT - Information Technology

- MitM - Man-in-the-middle

- ML - Machine Learning

- SQL - Structured Query Language

- VPN - Virtual Private Network